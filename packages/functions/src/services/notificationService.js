import {getOrderLimit, getProductById} from './shopifyService';
import {
  delNotification,
  createNotifications,
  getNotificationByShopifyDomain
} from '../repository/notificationRepository';

export const syncNotification = async ({shopifyDomain, accessToken, id}) => {
  const orderList = await getOrderLimit({accessToken, shopifyDomain});
  const notificationDoc = await getNotificationByShopifyDomain(shopifyDomain);
  if (notificationDoc) {
    await Promise.all(notificationDoc.map(doc => delNotification(doc.id)));
  }
  const notifications = await Promise.all(
    orderList.map(order =>
      processOrderItem({accessToken, shopifyDomain, order, id})
    )
  );

  await createNotifications(notifications);
};

async function processOrderItem({shopifyDomain, accessToken, order, id}) {
  const productId = order.line_items[0].product_id;
  const product = await getProductById({
    shopifyDomain,
    accessToken,
    productId
  });

  return {
    productId,
    productImage: product.image.src,
    productName: product.title,
    firstName: order.billing_address.first_name,
    country: order.billing_address.country,
    city: order.billing_address.city,
    createdAt: order.created_at,
    shopId: id,
    shopifyDomain
  };
}
