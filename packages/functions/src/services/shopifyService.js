import Shopify from 'shopify-api-node';
import {getShopByDomain} from '../repository/shopRepository';

export const shopifyInit = ({shopifyDomain, accessToken}) =>
  new Shopify({
    shopName: shopifyDomain,
    accessToken: accessToken
  });

export const getOrderLimit = ({shopifyDomain, accessToken, limit = 30}) => {
  return shopifyInit({shopifyDomain, accessToken}).order.list({limit});
};

export const getProductById = ({shopifyDomain, accessToken, productId}) => {
  return shopifyInit({shopifyDomain, accessToken}).product.get(productId);
};

export const createOrderWebhook = async ({shopifyDomain, accessToken}) => {
  const shopify = shopifyInit({shopifyDomain, accessToken});

  return shopify.webhook.create({
    address: 'https://7d59aef68799.ngrok.io/webhook/orders/new',
    topic: 'orders/create',
    format: 'json'
  });
};

export const getNotification = async ({shopifyDomain, data}) => {
  const {
    line_items: [{product_id: productId}],
    customer: {
      created_at,
      first_name,
      default_address: {city, country}
    }
  } = data;
  const {id: shopId, accessToken} = await getShopByDomain(shopifyDomain); // idShop, accessToken
  const product = await getProductById({shopifyDomain, accessToken, productId});

  return {
    shopId,
    city: city,
    country: country,
    firstName: first_name,
    productId,
    productImage: product.image.src,
    productName: product.title,
    shopifyDomain,
    createdAt: created_at
  };
};

export const createScripttag = async ({shopifyDomain, accessToken}) => {
  const shopify = shopifyInit({shopifyDomain, accessToken});

  return shopify.scriptTag.create({
    event: 'onload',
    src: 'https://localhost:3000/scripttag/avada-sale-pop.min.js'
  });
};

export const changeStatusPopup = async ({
  shopifyDomain,
  accessToken,
  status
}) => {
  const shopify = shopifyInit({shopifyDomain, accessToken});

  const themes = await shopify.theme.list({fields: 'id, role'});
  const {id: mainThemeId} = themes.filter(theme => theme.role === 'main')[0];

  const updateStatus = `{% assign popupStatus = '${
    status ? 'enable' : 'disable'
  }' %}`;
  return shopify.asset.update(mainThemeId, {
    key: 'snippets/avada-status-popup.liquid',
    value: updateStatus
  });
};
