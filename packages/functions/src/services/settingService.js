import {
  createNewSetting,
  isEmptySetting,
  removeOldSetting
} from '../repository/settingRepository';

const initialSetting = {
  excludedUrls: '',
  firstDelay: 3,
  hideTimeAgo: true,
  allowShow: 'all',
  includedUrls: '',
  popInterval: 3,
  truncateProductName: true,
  maxPopsDisplay: 3,
  position: 'bottom-left',
  displayDuration: 3
};

export async function addDefaultSetting(id) {
  const setting = await isEmptySetting(id);
  if (setting) {
    return createNewSetting({
      shopId: id,
      ...initialSetting
    });
  }

  const idSetting = setting.docs.id;
  await removeOldSetting(idSetting);
  return createNewSetting({
    shopId: id,
    ...initialSetting
  });
}
