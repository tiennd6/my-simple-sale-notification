import {db} from './index';

const settingCollection = db.collection('settings');

/**
 *
 * @param shopId
 * @returns {Promise<*&{id: string}>}
 */
export const getSetting = async shopId => {
  const setting = await settingCollection.where('shopId', '==', shopId).get();
  const settingDoc = setting.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));

  return settingDoc[0];
};

/**
 *
 * @param shopID
 * @returns {Promise<*&{id: string}>}
 */
export const isEmptySetting = async shopId => {
  return (await settingCollection.where('shopId', '==', shopId).get()).empty;
};

/**
 *
 * @param {} setting
 * @returns {Promise<FirebaseFirestore.DocumentReference>}
 */
export const createNewSetting = setting => settingCollection.add(setting);

/**
 *
 * @param data
 * @returns {Promise<FirebaseFirestore.WriteResult>}
 */
export const updateSetting = data => {
  const {id, ...settingData} = data;
  return settingCollection.doc(id).update(settingData);
};

/**
 *
 * @param idSetting
 * @returns {Promise<FirebaseFirestore.WriteResult>}
 */
export const removeOldSetting = idSetting =>
  settingCollection.doc(idSetting).delete();
