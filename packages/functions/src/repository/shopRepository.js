import {db} from './index';

const shopCollection = db.collection('shops');

export const getShopByDomain = async shopifyDomain => {
  const resp = await shopCollection
    .where('shopifyDomain', '==', shopifyDomain)
    .limit(1)
    .get();
  if (!resp.empty) {
    const shopDoc = resp.docs.map(doc => ({
      id: doc.id,
      ...doc.data()
    }));

    return shopDoc[0];
  }
  throw new Error('ShopId not found');
};

export const getAccessTokenShop = async shopId => {
  const resp = await shopCollection.doc(shopId).get();
  if (resp.exists) {
    const {accessToken, shopifyDomain} = resp.data();
    return {accessToken, shopifyDomain};
  }
  throw new Error('ShopId not found');
};

export const getShopById = async shopId => {
  const resp = await shopCollection.doc(shopId).get();
  if (resp.exists) {
    return {
      id: resp.id,
      ...resp.data()
    };
  }
  throw new Error('ShopId not found');
};

export const updateAppStatusShop = async ({shopId, status}) => {
  return shopCollection.doc(shopId).update({statusApp: status});
};
