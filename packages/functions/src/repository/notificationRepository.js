import {db} from './index';
import moment from 'moment';
import {omit} from '../helpers/utils';

const notificationCollection = db.collection('notifications');

export const getNotifications = async (key = 'desc') => {
  const resp = await notificationCollection.orderBy('createdAt', key).get();

  return resp.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
};

export const getNotificationByShopifyDomain = async shopifyDomain => {
  const notifications = await notificationCollection
    .where('shopifyDomain', '==', shopifyDomain)
    .get();
  if (notifications.empty) {
    return;
  }
  return notifications.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
};

export const getDocNotificationByShopifyDomain = async shopifyDomain => {
  const notifications = await notificationCollection
    .where('shopifyDomain', '==', shopifyDomain)
    .get();
  return notifications.docs.map(doc => {
    const pickedNotification = omit(doc.data());

    return {
      id: doc.id,
      ...pickedNotification,
      relativeDate: moment(doc.data().createdAt, 'YYYYMMDD').fromNow()
    };
  });
};

/**
 * get order => add notification to firestore
 * @param [...] orders
 * @returns {*}
 */
export const createNotifications = async notifications => {
  return Promise.all(
    notifications.map(notification => notificationCollection.add(notification))
  );
};

/**
 *
 * @param notification
 * @returns {Promise<FirebaseFirestore.DocumentReference>}
 */
export const createNewNotification = async notification => {
  return notificationCollection.add(notification);
};

export const delNotification = async id => {
  return await notificationCollection.doc(id).delete();
};
