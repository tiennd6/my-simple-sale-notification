import App from 'koa';
import webhookRouter from '../routes/webhook';

const webhookHandler = new App();
webhookHandler.proxy = true;

webhookHandler.use(webhookRouter.allowedMethods());
webhookHandler.use(webhookRouter.routes());

export default webhookHandler;
