import App from 'koa';
import clientApiRouter from '../routes/clientApi';
import cors from '@koa/cors';

const clientApiHandler = new App();
clientApiHandler.proxy = true;

clientApiHandler.use(cors());
clientApiHandler.use(clientApiRouter.allowedMethods());
clientApiHandler.use(clientApiRouter.routes());

export default clientApiHandler;
