const Shopify = require('shopify-api-node');
const serviceAccount = require('../config/serviceAccount.json');
const admin = require('firebase-admin');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

const notificationCollection = db.collection('notifications');

const shopify = new Shopify({
  shopName: 'avada-training-dev-2',
  accessToken: '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'
});

const enablePopup = `{% assign popupStatus = 'enable' %}`;

(async () => {
  try {
    const themes = await shopify.theme.list({fields: 'id, role'});
    const mainTheme = themes.filter(theme => theme.role === 'main')[0];
    const mainThemeId = mainTheme.id;

    const layoutThemeValue = await shopify.asset.update(mainThemeId, {
      key: 'snippets/avada-status-popup.liquid',
      value: enablePopup
    });

    console.log(layoutThemeValue);
    const other = await shopify.asset.get(mainThemeId, {
      'asset[key]': 'snippets/avada-status-popup.liquid',
      theme_id: mainThemeId
    });
    console.log(other)
  } catch (e) {
    console.log(e);
  }
})();
