import {getNotifications} from '../repository/notificationRepository';

export async function getAllNotification(ctx) {
  try {
    const {keySort} = ctx.query;
    const notification = await getNotifications(keySort);
    ctx.status = 200;

    return (ctx.body = {
      success: true,
      data: notification
    });
  } catch (e) {
    ctx.status = 400;
    return (ctx.body = {
      success: false,
      data: []
    });
  }
}
