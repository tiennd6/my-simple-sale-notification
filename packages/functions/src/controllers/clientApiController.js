import {getDocNotificationByShopifyDomain} from '../repository/notificationRepository';
import {getSetting} from '../repository/settingRepository';
import {getShopByDomain} from '../repository/shopRepository';

export async function getNotificationSetting(ctx) {
  try {
    const {shopifyDomain} = ctx.query;
    const {id} = await getShopByDomain(shopifyDomain);
    const [notifications, settings] = await Promise.all([
      getDocNotificationByShopifyDomain(shopifyDomain),
      getSetting(id)
    ]);
    ctx.status = 200;

    return (ctx.body = {
      notifications,
      settings
    });
  } catch (e) {
    ctx.status = 400;
    return (ctx.body = {
      success: false,
      data: [],
      error: e
    });
  }
}
