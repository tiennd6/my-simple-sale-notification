import {getSetting, updateSetting} from '../repository/settingRepository';
import {getCurrentUserInstance} from '../helpers/auth';

export async function getAllSetting(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const setting = await getSetting(shopID);

    return (ctx.body = {
      success: true,
      data: setting
    });
  } catch (e) {
    ctx.status = 404;
    return (ctx.body = {
      success: false,
      data: {}
    });
  }
}

export const updateSettingData = async ctx => {
  try {
    const data = ctx.req.body;
    await updateSetting(data);

    return (ctx.body = {
      success: true,
      data
    });
  } catch (e) {
    ctx.status = 404;
    return (ctx.body = {
      success: false,
      data: {}
    });
  }
};
