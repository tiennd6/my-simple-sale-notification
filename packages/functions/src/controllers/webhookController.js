import {getNotification} from '../services/shopifyService';
import {createNewNotification} from '../repository/notificationRepository';

export const getNewNotification = async ctx => {
  try {
    const data = ctx.req.body;
    const shopifyDomain = ctx.get('x-shopify-shop-domain');
    const notification = await getNotification({shopifyDomain, data});
    await createNewNotification(notification);
    ctx.status = 200;

    return (ctx.body = {
      success: true,
      data: notification
    });
  } catch (e) {
    console.error(e);
    ctx.status = 200;
    return (ctx.body = {
      success: false,
      data: {},
      error: e
    });
  }
};
