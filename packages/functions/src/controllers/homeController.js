import {changeStatusPopup} from '../services/shopifyService';
import {getCurrentUserInstance} from '../helpers/auth';
import {
  getAccessTokenShop,
  updateAppStatusShop,
  getShopById
} from '../repository/shopRepository';

export const handleChangeStatusPopup = async ctx => {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const {accessToken, shopifyDomain} = await getAccessTokenShop(shopID);
    const {status} = ctx.req.body;
    await updateAppStatusShop({shopId: shopID, status});
    await changeStatusPopup({shopifyDomain, accessToken, status});

    return (ctx.body = {
      success: true
    });
  } catch (e) {
    ctx.status = 400;
    return (ctx.body = {
      success: false,
      data: {},
      error: e
    });
  }
};

export const getStatusPopup = async ctx => {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const {statusApp} = await getShopById(shopID);

    return (ctx.body = {
      success: true,
      data: statusApp
    });
  } catch (e) {
    ctx.status = 400;
    return (ctx.body = {
      success: false,
      data: {},
      error: e
    });
  }
};
