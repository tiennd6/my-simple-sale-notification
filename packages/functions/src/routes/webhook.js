import Router from 'koa-router';
import * as webhookController from '../controllers/webhookController';

const webhookRouter = new Router({
  prefix: '/webhook'
});

webhookRouter.post('/orders/new', webhookController.getNewNotification);

export default webhookRouter;
