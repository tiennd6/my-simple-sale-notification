import Router from 'koa-router';
import * as settingController from '../controllers/settingController';
import * as notificationController from '../controllers/notificationController';
import * as homeController from '../controllers/homeController';
import {verifyRequest} from '@avada/shopify-auth';

const router = new Router({
  prefix: '/api'
});

router.use(verifyRequest());

router.get('/home', homeController.getStatusPopup);
router.post('/home', homeController.handleChangeStatusPopup);
router.get('/settings', settingController.getAllSetting);
router.put('/settings', settingController.updateSettingData);
router.get('/notifications', notificationController.getAllNotification);

export default router;
