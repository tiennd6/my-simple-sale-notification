import Router from 'koa-router';
import * as clientApiController from '../controllers/clientApiController';

const clientApiRouter = new Router({
  prefix: '/clientApi'
});

clientApiRouter.get(
  '/notifications',
  clientApiController.getNotificationSetting
);

export default clientApiRouter;
