export const delayDurationProcess = ms => {
  return new Promise(res => setTimeout(res, ms));
};
