import React from 'react';
import {truncateString} from '../../helpers/truncateString';
import './NoticationPopup.scss';

const NotificationPopup = ({
  firstName = 'John Doe',
  city = 'New York',
  country = 'United States',
  productName = 'Puffer Jacket With Hidden Hood',
  relativeDate = 'a day ago',
  productImage = 'http://paris.mageplaza.com/images/shop/single/big-1.jpg',
  hideTimeAgo = false,
  truncateProductName = false,
  position = 'bottom-left'
}) => {
  const getPosition = position => {
    if (position === 'top-left') {
      return 'Avava-SP__Container--Top-Left';
    }
    if (position === 'bottom-left') {
      return 'Avava-SP__Container--Bottom-Left';
    }
    if (position === 'top-right') {
      return 'Avava-SP__Container--Top-Right';
    }
    if (position === 'bottom-right') {
      return 'Avava-SP__Container--Bottom-Right';
    }
  };

  return (
    <div className="Avava-SP__Wrapper">
      <div className="Avava-SP__Inner">
        <div className={getPosition(position)}>
          <a href="#" className={'Avava-SP__LinkWrapper'}>
            <div
              className="Avava-SP__Image"
              style={{
                backgroundImage: `url(${productImage})`
              }}
            />
            <div className="Avada-SP__Content">
              <div className={'Avada-SP__Title'}>
                {firstName} in {city}, {country}
              </div>
              <div className={'Avada-SP__Subtitle'}>
                Purchased{' '}
                {truncateProductName
                  ? truncateString(productName, 20)
                  : productName}
              </div>
              <div className={'Avada-SP__Footer'}>
                {hideTimeAgo ? '' : relativeDate}{' '}
                <span className="uni-blue">
                  <i className="fa fa-check" aria-hidden="false" /> by Avada
                </span>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

NotificationPopup.propTypes = {};

export default NotificationPopup;
