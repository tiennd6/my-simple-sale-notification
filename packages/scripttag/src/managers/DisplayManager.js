import {insertAfter} from '../helpers/insertHelpers';
import {render} from 'preact';
import React from 'preact/compat';
import NotificationPopup from '../components/NotificationPopup/NotificationPopup';
import {delayDurationProcess} from '../services/displayService';

export default class DisplayManager {
  constructor() {
    this.notifications = [];
    this.settings = {};
  }

  async initialize({notifications, settings}) {
    // Todo: With notification and settings, update the displaying logic
    this.notifications = notifications;
    this.settings = settings;
    this.insertContainer();
    if (this.checkNotDisplay()) return;
    await this.startDisplay();
  }

  async startDisplay() {
    await delayDurationProcess(this.settings.firstDelay * 1000);
    const {maxPopDisplay} = this.settings;
    const notification = this.notifications.slice(0, maxPopDisplay);
    for (const item of notification) {
      await this.displayOnePopup(item, this.settings.displayDuration * 1000);
      this.fadeOut();
      await delayDurationProcess(this.settings.popInterval * 1000);
    }
  }

  checkNotDisplay() {
    const url = window.location.href;
    const includePage = this.settings.includedUrls.split('\n');
    const excludePage = this.settings.excludedUrls.split('\n');
    const allowShow = this.settings.allowShow;

    if (allowShow === 'all') {
      return excludePage.indexOf(url) >= 0;
    }
    if (allowShow === 'specific') {
      return includePage.indexOf(url) < 0;
    }

    return true;
  }

  async displayOnePopup(notification, duration = 1000) {
    const settings = this.settings;
    const container = document.querySelector('#Avada-SalePop');
    const data = {
      ...notification,
      hideTimeAgo: settings.hideTimeAgo,
      truncateProductName: settings.truncateProductName,
      position: settings.position
    };
    render(<NotificationPopup {...data} />, container);
    await delayDurationProcess(duration);
  }

  fadeOut() {
    const container = document.querySelector('#Avada-SalePop');
    container.innerHTML = '';
  }

  insertContainer() {
    const popupEl = document.createElement('div');
    popupEl.id = `Avada-SalePop`;
    popupEl.classList.add('Avada-SalePop__OuterWrapper');
    const targetEl = document.querySelector('body').firstChild;
    if (targetEl) {
      insertAfter(popupEl, targetEl);
    }

    return popupEl;
  }
}
