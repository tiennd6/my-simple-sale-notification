import React, {useState} from 'react';
import {
  Page,
  Card,
  ResourceItem,
  ResourceList,
  Pagination,
  Layout,
  Stack,
  FormLayout,
  SkeletonBodyText
} from '@shopify/polaris';
import useFetchApi from '../../hooks/useFetchApi';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import {parseDateTime} from '../../utils/parseDateTime';

export default function Notification() {
  const [sortValue, setSortValue] = useState('desc');
  const {notifications, setting, fetched, loading, fetchApi} = useFetchApi();

  const handleOnSortChange = async selected => {
    setSortValue(selected);
    await fetchApi({
      url: '/notifications',
      method: 'GET',
      data: {},
      params: {keySort: selected}
    });
  };

  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };

  const renderItem = notifications => {
    const {id, createdAt} = notifications;

    const data = {
      ...setting,
      ...notifications,
      fetched
    };

    return (
      <ResourceItem id={id}>
        <Stack alignment="center">
          <Stack.Item fill>
            <NotificationPopup {...data} />
          </Stack.Item>
          <Stack.Item>{parseDateTime(createdAt)}</Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  return (
    <Page
      fullWidth
      title="Notifications"
      subtitle="List of sale notification from Shopify"
    >
      {fetched ? (
        <FormLayout>
          <Card>
            <ResourceList
              resourceName={resourceName}
              items={notifications}
              renderItem={renderItem}
              sortValue={sortValue}
              sortOptions={[
                {label: 'Newest update', value: 'desc'},
                {label: 'Oldest update', value: 'asc'}
              ]}
              onSortChange={handleOnSortChange}
              loading={loading}
            />
          </Card>
          <br />
          <Layout>
            <Pagination
              hasPrevious
              onPrevious={() => {
                console.log('Previous');
              }}
              hasNext
              onNext={() => {
                console.log('Next');
              }}
            />
          </Layout>
        </FormLayout>
      ) : (
        <SkeletonBodyText lines={10} />
      )}
    </Page>
  );
}
