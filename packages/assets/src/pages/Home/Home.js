import React, {useCallback} from 'react';
import {Page, SettingToggle, TextStyle} from '@shopify/polaris';
import useFetchApi from '../../hooks/useFetchApi';

/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {
  const {status, setStatus, fetchApi, loading} = useFetchApi();

  const handleToggle = useCallback(() => {
    setStatus(prevState => !prevState);
  }, []);
  const contentStatus = status ? 'Disable' : 'Enable';
  const textStatus = status ? 'enabled' : 'disabled';

  const handleSubmit = async () => {
    await fetchApi({url: '/home', method: 'POST', data: {status}});
  };

  return (
    <Page
      fullWidth
      title="Home"
      primaryAction={{
        content: 'Save',
        loading,
        onAction: handleSubmit
      }}
    >
      <SettingToggle
        action={{
          content: contentStatus,
          onAction: handleToggle
        }}
        enabled={status}
      >
        App status is <TextStyle variation="strong">{textStatus}</TextStyle>.
      </SettingToggle>
    </Page>
  );
}
