import React, {useState, useCallback} from 'react';
import {Card, Layout, Page, Tabs, SkeletonBodyText} from '@shopify/polaris';
import TriggerTab from '../../components/triggerTab';
import DisplayTab from '../../components/displayTab';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import useFetchApi from '../../hooks/useFetchApi';

export default function Settings() {
  const [selectedTab, setSelectedTab] = useState(0);
  const {
    setting,
    notifications,
    loading,
    fetched,
    setSetting,
    fetchApi
  } = useFetchApi();

  const handleDisplayTab = useCallback((key, value) => {
    setSetting(prevState => ({...prevState, [key]: value}));
  }, []);

  const handleTriggerTab = useCallback((key, value) => {
    setSetting(prevState => ({...prevState, [key]: value}));
  }, []);

  const handleTabSelected = useCallback(
    selectedTabIndex => setSelectedTab(selectedTabIndex),
    []
  );

  const handleSubmitSetting = useCallback(() => {
    fetchApi({url: '/settings', method: 'PUT', data: setting});
  }, [setting]);

  const data = {
    ...setting,
    ...notifications[0],
    fetched
  };

  const tabs = [
    {
      id: 'Display',
      content: 'Display',
      section: 'APPEARANCE',
      renderElm: <DisplayTab data={setting} handleDisplay={handleDisplayTab} />
    },
    {
      id: 'Triggers',
      content: 'Triggers',
      section: 'PAGE RESTRICTION',
      renderElm: <TriggerTab data={setting} handleTrigger={handleTriggerTab} />
    }
  ];

  return (
    <Page
      fullWidth
      title="Settings"
      subtitle="Decide how your notifications will display"
      primaryAction={{
        content: 'Save',
        loading,
        onAction: handleSubmitSetting
      }}
    >
      <Layout>
        <Layout.AnnotatedSection title={<NotificationPopup {...data} />}>
          <Card>
            <Tabs
              tabs={tabs}
              selected={selectedTab}
              onSelect={handleTabSelected}
            >
              <Card.Section title={tabs[selectedTab].section}>
                {fetched ? (
                  tabs[selectedTab].renderElm
                ) : (
                  <SkeletonBodyText lines={10} />
                )}
              </Card.Section>
            </Tabs>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
}
