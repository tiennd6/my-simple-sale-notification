import React from 'react';
import {truncateString} from '../../utils/truncateString';
import {SkeletonBodyText} from '@shopify/polaris';
import './NoticationPopup.scss';
import {relativeDate} from '../../utils/parseDateTime';

const NotificationPopup = ({
  firstName,
  city,
  country,
  productImage,
  productName,
  hideTimeAgo,
  truncateProductName,
  fetched,
  createdAt
}) => {
  return (
    <div className="Avava-SP__Wrapper">
      <div className="Avava-SP__Inner">
        {fetched ? (
          <div className="Avava-SP__Container">
            <a href="#" className={'Avava-SP__LinkWrapper'}>
              <div
                className="Avava-SP__Image"
                style={{
                  backgroundImage: `url(${productImage})`
                }}
              />
              <div className="Avada-SP__Content">
                <div className={'Avada-SP__Title'}>
                  {firstName} in {city}, {country}
                </div>
                <div className={'Avada-SP__Subtitle'}>
                  Purchased{' '}
                  {truncateProductName
                    ? truncateString(productName, 20)
                    : productName}
                </div>
                <div className={'Avada-SP__Footer'}>
                  {hideTimeAgo ? '' : relativeDate(createdAt)}{' '}
                  <span className="uni-blue">
                    <i className="fa fa-check" aria-hidden="false" /> by Avada
                  </span>
                </div>
              </div>
            </a>
          </div>
        ) : (
          <SkeletonBodyText lines={3} />
        )}
      </div>
    </div>
  );
};

NotificationPopup.propTypes = {};

export default NotificationPopup;
