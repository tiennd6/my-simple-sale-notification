import React from 'react';
import {Select, TextField, FormLayout} from '@shopify/polaris';
import PropTypes from 'prop-types';

export default function TriggerTab({data, handleTrigger}) {
  const options = [
    {label: 'All Page', value: 'all'},
    {label: 'Specific pages', value: 'specific'}
  ];

  return (
    <FormLayout>
      <Select
        options={options}
        onChange={value => handleTrigger('allowShow', value)}
        value={data.allowShow}
      />
      {data.allowShow === 'specific' ? (
        <div>
          <TextField
            label="Included pages"
            value={data.includedUrls}
            onChange={value => handleTrigger('includedUrls', value)}
            multiline={3}
            helpText="Page URLs to show the pop-up (separated by new lines)"
          />
        </div>
      ) : null}
      <TextField
        label="Excluded pages"
        value={data.excludedUrls}
        onChange={value => handleTrigger('excludedUrls', value)}
        multiline={3}
        helpText="Page URLs NOT to show the pop-up (separated by new lines)"
      />
    </FormLayout>
  );
}

TriggerTab.propTypes = {
  data: PropTypes.object,
  handleTrigger: PropTypes.func
};
