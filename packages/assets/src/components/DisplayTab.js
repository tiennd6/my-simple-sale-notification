import React, {Fragment} from 'react';
import {Checkbox, FormLayout, RangeSlider, TextField} from '@shopify/polaris';
import DesktopPositionInput from './DesktopPositionInput';
import PropTypes from 'prop-types';

export default function DisplayTab({data, handleDisplay}) {
  const suffixStyles = {
    maxWidth: '105px',
    textAlign: 'right'
  };

  const {
    displayDuration,
    firstDelay,
    popInterval,
    maxPopsDisplay,
    position,
    hideTimeAgo,
    truncateProductName
  } = data;

  const timeDisplay = [
    {
      id: 'displayDuration',
      labelSlider: 'Display duration',
      minValue: 0,
      maxValue: 30,
      rangeValue: displayDuration,
      helpText: 'How long each pop will display on your page.'
    },
    {
      id: 'firstDelay',
      labelSlider: 'Time before the first pop',
      minValue: 0,
      maxValue: 30,
      rangeValue: firstDelay,
      helpText: 'The delay time before the first nofication.'
    },
    {
      id: 'popInterval',
      labelSlider: 'Gap time between two pops',
      minValue: 0,
      maxValue: 30,
      rangeValue: popInterval,
      helpText: 'The time interval between two popup nofication.'
    },
    {
      id: 'maxPopsDisplay',
      labelSlider: 'Maximum of popups',
      minValue: 0,
      maxValue: 80,
      rangeValue: maxPopsDisplay,
      helpText:
        'The maximum number of popups are allowed to show after page loading. Maximum number is 80'
    }
  ];

  const rangeSlider = id => {
    const rangeElm = timeDisplay.find(elm => elm.id === id);
    return (
      <RangeSlider
        key={rangeElm.id}
        id={rangeElm.id}
        output
        label={rangeElm.labelSlider}
        min={rangeElm.minValue}
        max={rangeElm.maxValue}
        value={rangeElm.rangeValue}
        onChange={value => handleDisplay(id, parseInt(value))}
        suffix={
          <div style={suffixStyles}>
            <TextField
              label=""
              value={rangeElm.rangeValue.toString()}
              onChange={value => handleDisplay(rangeElm.id, parseInt(value))}
              suffix="second(s)"
              labelHidden
            />
          </div>
        }
        helpText={rangeElm.helpText}
      />
    );
  };

  return (
    <Fragment>
      <DesktopPositionInput
        value={position}
        label={`Desktop Postion`}
        helpText={`The display position of the pop in your website.`}
        onChange={handleDisplay}
      />
      <br />
      <FormLayout>
        <Checkbox
          label="Hide time ago"
          checked={hideTimeAgo}
          onChange={newChecked => handleDisplay('hideTimeAgo', newChecked)}
        />
        <Checkbox
          label="Truncate context text"
          checked={truncateProductName}
          onChange={newChecked =>
            handleDisplay('truncateProductName', newChecked)
          }
          helpText={`If your produt name is long for one line. it will be trunated to 'Product na...'`}
        />
      </FormLayout>
      <br />
      <FormLayout>
        <FormLayout.Group>
          {rangeSlider('displayDuration')}
          {rangeSlider('firstDelay')}
        </FormLayout.Group>
        <FormLayout.Group>
          {rangeSlider('popInterval')}
          {rangeSlider('maxPopsDisplay')}
        </FormLayout.Group>
      </FormLayout>
    </Fragment>
  );
}

DisplayTab.propTypes = {
  data: PropTypes.object,
  handleDisplay: PropTypes.func
};
