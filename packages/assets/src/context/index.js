import React, {createContext, useEffect, useReducer} from 'react';
import PropTypes from 'prop-types';
import {api} from '../helpers';

const Context = createContext({});

Context.displayName = 'FETCH_CONTEXT';

const initialState = {
  loading: false,
  fetched: false,
  setting: {},
  notifications: []
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {...state, loading: action.payload};
    case 'SET_SETTING': {
      const setting = action.payload.data;
      return {...state, setting};
    }
    case 'SET_FETCHED':
      return {...state, fetched: action.payload};
    case 'SET_NOTIFICATION': {
      const notifications = action.payload.data;
      return {
        ...state,
        notifications
      };
    }
  }
};

export const ContextProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    (async () => {
      try {
        dispatch({type: 'SET_LOADING', payload: true});
        const [respNotification, respSetting] = await Promise.all([
          api('/notifications'),
          api('/settings')
        ]);
        if (respNotification.success === true || respSetting.success === true) {
          dispatch({type: 'SET_SETTING', payload: respSetting});
          dispatch({type: 'SET_NOTIFICATION', payload: respNotification});
          dispatch({type: 'SET_FETCHED', payload: true});
        }
      } catch (e) {
        console.error('error ', e);
      } finally {
        dispatch({type: 'SET_LOADING', payload: false});
      }
    })();
  }, []);

  return (
    <Context.Provider value={{state, dispatch}}>{children}</Context.Provider>
  );
};

ContextProvider.propTypes = {
  children: PropTypes.element
};

export default Context;
