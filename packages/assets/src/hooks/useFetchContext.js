import FetchContext from '../context';
import {useContext} from 'react';
import {api} from '../helpers';

const useFetchContext = () => {
  const context = useContext(FetchContext);

  const {dispatch} = context;

  const sortDateNotification = async ({url, method, data, params}) => {
    try {
      console.log(url, method, data, params);
      dispatch({type: 'SET_LOADING', payload: true});
      const resp = await api(url, method, data, params);
      if (resp.success) {
        dispatch({type: 'SET_NOTIFICATION', payload: resp});
      }
    } catch (e) {
      console.log('error ', e);
    } finally {
      dispatch({type: 'SET_LOADING', payload: false});
    }
  };

  return {...context, service: {sortDateNotification}};
};

export default useFetchContext;
