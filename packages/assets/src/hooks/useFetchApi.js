import {useState, useEffect} from 'react';
import {api} from '../helpers';
import {store} from '../index';
import {setToast} from '../actions/layout/setToastAction';

export default function useFetchApi() {
  const [setting, setSetting] = useState({});
  const [status, setStatus] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(false);
  const [fetched, setFetched] = useState(false);

  const loadData = async () => {
    const [respHome, respSetting, respNotifications] = await Promise.all([
      api('/home'),
      api('/settings'),
      api('/notifications')
    ]);
    if (respHome.success || respNotifications.success || respSetting.success) {
      setStatus(respHome.data);
      setSetting(respSetting.data);
      setNotifications(respNotifications.data);
      setFetched(true);
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  const fetchApi = async ({
    url = '',
    method = 'GET',
    data = {},
    params = {}
  }) => {
    try {
      setLoading(true);
      const resp = await api(url, method, data, params);
      if (url === '/home' && resp.success) {
        store.dispatch(
          setToast({
            content: 'Update status successfully'
          })
        );
      }
      if (url === '/settings' && resp.success) {
        setSetting(resp.data);
        store.dispatch(
          setToast({
            content: 'Update setting successfully'
          })
        );
      }
      if (url === '/notifications' && resp.success) {
        setNotifications(resp.data);
        store.dispatch(
          setToast({
            content: 'Sort date successfully'
          })
        );
      }
    } catch (e) {
      store.dispatch(
        setToast({
          content: 'Error updating',
          error: true
        })
      );
      console.error('error ', e);
    } finally {
      setLoading(false);
    }
  };

  return {
    status,
    setStatus,
    setting,
    setSetting,
    notifications,
    loading,
    fetched,
    fetchApi
  };
}
