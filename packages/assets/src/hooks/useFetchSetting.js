import {useEffect, useState} from 'react';
import {api} from '../helpers';
import {store} from '../index';
import {setToast} from '../actions/layout/setToastAction';

export default function useFetchSetting({
  url = '',
  method = 'GET',
  data = {},
  param = {}
}) {
  const [fetched, setFetched] = useState(false);
  const [loading, setLoading] = useState(false);
  const [setting, setSetting] = useState({});
  const [notification, setNotification] = useState([]);

  const updateSetting = async data => {
    try {
      setLoading(true);
      const resp = await api('/settings', 'PUT', data);
      if (resp.success === true) {
        setSetting(resp.data);
        store.dispatch(
          setToast({
            content: 'Update setting successfully',
            error
          })
        );
      }
    } catch (e) {
      console.log('error ', e);
    } finally {
      setLoading(false);
    }
  };

  const sortDateNotification = async key => {
    try {
      setLoading(true);
      const resp = await api('/notifications', 'GET', {}, {keySort: key});
      if (resp.success === true) {
        setNotification(resp.data);
      }
    } catch (e) {
      console.log('error ', e);
    } finally {
      setLoading(false);
    }
  };

  async function loadData() {
    try {
      setLoading(true);
      const [respNotifi, respSetting] = await Promise.all([
        api('/notifications'),
        api('/settings')
      ]);
      if (respNotifi.success === true || respSetting.success === true) {
        setSetting(respSetting.data);
        setNotification(respNotifi.data);
        setFetched(true);
      }
    } catch (e) {
      console.error('error ', e);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    loadData();
  }, []);

  return {
    setting,
    setSetting,
    notification,
    loading,
    fetched,
    updateSetting,
    sortDateNotification
  };
}
