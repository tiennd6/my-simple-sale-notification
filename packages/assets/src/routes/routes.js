import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '../loadables/Home';
import Notifications from '../loadables/Notifications';
import Settings from '../loadables/Settings';
import NotFound from '../loadables/NotFound';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/settings" component={Settings} />
    <Route exact path="/notifications" component={Notifications} />
    <Route path="*" component={NotFound} />
  </Switch>
);

export default Routes;
