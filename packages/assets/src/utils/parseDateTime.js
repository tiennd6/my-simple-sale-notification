import moment from 'moment';

const monthConvert = [
  'Jan',
  'Feb',
  'March',
  'April',
  'May',
  'June',
  'July',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec'
];
export const parseDateTime = date => {
  const timestamp = new Date(date);
  const year = timestamp.getFullYear();
  const monthNum = timestamp.getMonth();
  const month = monthConvert[monthNum];
  const day = timestamp.getDate();
  return `From ${month} ${day}, ${year}`;
};

export const relativeDate = date => {
  return moment(date, 'YYYYMMDD').fromNow();
};
