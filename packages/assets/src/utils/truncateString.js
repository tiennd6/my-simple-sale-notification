export const truncateString = (str, num) => {
  if (!str) return;
  if (str.length > num) {
    return str.slice(0, num) + '...';
  }
  return str;
};
